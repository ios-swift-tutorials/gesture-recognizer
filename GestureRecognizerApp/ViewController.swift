//
//  ViewController.swift
//  GestureRecognizerApp
//
//  Created by Rumeysa Bulut on 11.11.2019.
//  Copyright © 2019 Rumeysa Bulut. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var myLabel: UILabel!
    var isApple = true
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.isUserInteractionEnabled = true
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(changePic))
        
        imageView.addGestureRecognizer(gestureRecognizer)
    }

    @objc func changePic(){
        if isApple == true {
            imageView.image = UIImage(named: "android")
            myLabel.text = "Android Logo"
            isApple = false
        }
        else {
            imageView.image = UIImage(named: "apple")
            myLabel.text = "Apple Logo"
            isApple = true
        }
        
    }

}

